package de.parkandmore

import com.sun.istack.internal.Nullable;

class User {
	
	String firstName
	String fastName
	Date birtdate
	Address address
	static hasOne = [address: Address]

    static constraints = {
		 firstName(nullable:false)
		 fastName(nullable:false)
		 birtdate(nullable:false)
		 address(Nullable:false)
    }
}
