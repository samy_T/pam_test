package de.parkandmore

import org.apache.commons.lang.builder.ToStringBuilder;

class Address {
	String city
	String zipCode
	String street
	String houseNumber
	String telephone
	String mobile
	static belongsTo = [user:User]
	
    @Override
	public String toString() {
		return new ToStringBuilder(this).
		append("city", city).
		append("zipCode", zipCode).
		append("street", street).
		append("telephone", telephone).
		append("mobile", mobile).
		toString();

	}


	static constraints = {
		 city(nullable:false)
		 zipCode()
		 street()
		 houseNumber()
		 telephone()
	     mobile()
    }
}
