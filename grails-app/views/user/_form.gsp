<%@ page import="de.parkandmore.User" %>



<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'firstName', 'error')} ">
	<label for="firstName">
		<g:message code="user.firstName.label" default="First Name" />
		
	</label>
	<g:textField name="firstName" value="${userInstance?.firstName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'fastName', 'error')} ">
	<label for="fastName">
		<g:message code="user.fastName.label" default="Fast Name" />
		
	</label>
	<g:textField name="fastName" value="${userInstance?.fastName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'birtdate', 'error')} required">
	<label for="birtdate">
		<g:message code="user.birtdate.label" default="Birtdate" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="birtdate" precision="day"  value="${userInstance?.birtdate}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="address.city.label" default="City" />
		
	</label>
	<g:textField name="address.city" value="${userInstance?.address?.city}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'zipCode', 'error')} ">
	<label for="zipCode">
		<g:message code="address.zipCode.label" default="Zip Code" />
		
	</label>
	<g:textField name="address.zipCode" value="${userInstance?.address?.zipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'street', 'error')} ">
	<label for="street">
		<g:message code="address.street.label" default="Street" />
		
	</label>
	<g:textField name="address.street" value="${userInstance?.address?.street}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'houseNumber', 'error')} ">
	<label for="houseNumber">
		<g:message code="address.houseNumber.label" default="House Number" />
		
	</label>
	<g:textField name="address.houseNumber" value="${userInstance?.address?.houseNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="address.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="address.telephone" value="${userInstance?.address?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance.address, field: 'mobile', 'error')} ">
	<label for="mobile">
		<g:message code="address.mobile.label" default="Mobile" />
		
	</label>
	<g:textField name="address.mobile" value="${userInstance?.address?.mobile}"/>
</div>

<%--<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'address', 'error')} required">--%>
<%--	<label for="address">--%>
<%--		<g:message code="user.address.label" default="Address" />--%>
<%--		<span class="required-indicator">*</span>--%>
<%--	</label>--%>
<%--	<g:select id="address" name="address.id" from="${de.parkandmore.Address.list()}" optionKey="id" required="" value="${userInstance?.address?.id}" class="many-to-one"/>--%>
<%--</div>--%>

