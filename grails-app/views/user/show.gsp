
<%@ page import="de.parkandmore.User" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-user" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-user" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list user">
			
				<g:if test="${userInstance?.firstName}">
				<li class="fieldcontain">
					<span id="firstName-label" class="property-label"><g:message code="user.firstName.label" default="First Name" /></span>
					
						<span class="property-value" aria-labelledby="firstName-label"><g:fieldValue bean="${userInstance}" field="firstName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userInstance?.fastName}">
				<li class="fieldcontain">
					<span id="fastName-label" class="property-label"><g:message code="user.fastName.label" default="Fast Name" /></span>
					
						<span class="property-value" aria-labelledby="fastName-label"><g:fieldValue bean="${userInstance}" field="fastName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${userInstance?.birtdate}">
				<li class="fieldcontain">
					<span id="birtdate-label" class="property-label"><g:message code="user.birtdate.label" default="Birtdate" /></span>
					
						<span class="property-value" aria-labelledby="birtdate-label"><g:formatDate date="${userInstance?.birtdate}" /></span>
					
				</li>
				</g:if>		
			
<%--				<g:if test="${userInstance?.address?.city}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="city-label" class="property-label"><g:message code="address.city.label" default="City" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="city-label"><g:fieldValue bean="${userInstance}" field="address.city"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${userInstance?.address?.zipCode}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="zipCode-label" class="property-label"><g:message code="address.zipCode.label" default="Zip Code" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="zipCode-label"><g:fieldValue bean="${userInstance}" field="address.zipCode"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${userInstance?.address?.street}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="street-label" class="property-label"><g:message code="address.street.label" default="Street" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="street-label"><g:fieldValue bean="${userInstance}" field="address.street"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${userInstance?.address?.houseNumber}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="houseNumber-label" class="property-label"><g:message code="address.houseNumber.label" default="House Number" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="houseNumber-label"><g:fieldValue bean="${userInstance}" field="address.houseNumber"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${userInstance?.address?.address?.telephone}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="telephone-label" class="property-label"><g:message code="address.telephone.label" default="Telephone" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="telephone-label"><g:fieldValue bean="${userInstance}" field="address.telephone"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
<%--				<g:if test="${userInstance?.address?.mobile}">--%>
<%--				<li class="fieldcontain">--%>
<%--					<span id="mobile-label" class="property-label"><g:message code="address.mobile.label" default="Mobile" /></span>--%>
<%--					--%>
<%--						<span class="property-value" aria-labelledby="mobile-label"><g:fieldValue bean="${userInstance}" field="address.mobile"/></span>--%>
<%--					--%>
<%--				</li>--%>
<%--				</g:if>--%>
<%--			--%>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${userInstance?.id}" />
					<g:link class="edit" action="edit" id="${userInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
