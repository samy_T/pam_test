<%@ page import="de.parkandmore.Address" %>



<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'city', 'error')} ">
	<label for="city">
		<g:message code="address.city.label" default="City" />
		
	</label>
	<g:textField name="city" value="${addressInstance?.city}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'zipCode', 'error')} ">
	<label for="zipCode">
		<g:message code="address.zipCode.label" default="Zip Code" />
		
	</label>
	<g:textField name="zipCode" value="${addressInstance?.zipCode}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'street', 'error')} ">
	<label for="street">
		<g:message code="address.street.label" default="Street" />
		
	</label>
	<g:textField name="street" value="${addressInstance?.street}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'houseNumber', 'error')} ">
	<label for="houseNumber">
		<g:message code="address.houseNumber.label" default="House Number" />
		
	</label>
	<g:textField name="houseNumber" value="${addressInstance?.houseNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'telephone', 'error')} ">
	<label for="telephone">
		<g:message code="address.telephone.label" default="Telephone" />
		
	</label>
	<g:textField name="telephone" value="${addressInstance?.telephone}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field:'mobile', 'error')} ">
	<label for="mobile">
		<g:message code="address.mobile.label" default="Mobile" />
		
	</label>
	<g:textField name="mobile" value="${addressInstance?.mobile}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: addressInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="address.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${de.parkandmore.User.list()}" optionKey="id" required="" value="${addressInstance?.user?.id}" class="many-to-one"/>
</div>

