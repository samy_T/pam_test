dataSource {
 //   pooled = true
//    driverClassName = "org.h2.Driver"
//    username = "sa"
//    password = ""
	pooled = true
	driverClassName ="com.mysql.jdbc.Driver"//"com.cloudbees.jdbc.Driver"
	dialect = "org.hibernate.dialect.MySQL5InnoDBDialect"
	loggingSql = true
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
//    development {
//        dataSource {
//            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
//            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
//        }
//    }
	//grails -Dgrails.env=sama-local run-app
	development {
		dataSource {
			dbCreate = "create-drop" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost/pam?autoreconnect=true&useUnicode=yes&characterEncoding=UTF-8"
			username = "pam"
			password = "qwpam"
		}
		hibernate {
			show_sql = true
		}
	}
	
//	development {
//		dataSource {
//			dbCreate = "create-drop" // one of 'create', 'create-drop','update'
//			url = "jdbc:cloudbees://pam-test?autoreconnect=true&useUnicode=yes&characterEncoding=UTF-8"
//			username = "pam-test"
//			password = "qwpam"
//		}
//		hibernate {
//			show_sql = true
//		}
//	}
	
    test {
        dataSource {
            dbCreate = "update"
			url = "jdbc:cloudbees://pam-test?useUnicode=yes&characterEncoding=UTF-8"
			username = "pam-test"
			password = "qwpam"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:cloudbees://pam-test?useUnicode=yes&characterEncoding=UTF-8"
			username = "pam-test"
			password = "qwpam"
            pooled = true
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=true
               validationQuery="SELECT 1"
            }
        }
    }
}
